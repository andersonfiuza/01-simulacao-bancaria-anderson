package com.company;

import java.util.Map;

public class Main {

    public static void main(String[] args) {

        IO.imprimirMensagemInicial();
        Map<String, String> dados = IO.solicitarDados();

        Cliente cliente = new Cliente
                (
                        dados.get("nome"),
                        dados.get("cpf"),
                        Integer.parseInt(dados.get("idade")),
                        Integer.parseInt(dados.get("escolha"))

                );

        if (cliente.verificaiIdade() == true) {


            if (cliente.verificaAcao() == true) {

                Conta saca = new Conta();
                saca.setAgencia(1500);
                saca.setConta(99);
                saca.setDac("9");
                saca.setValor((float) 75.0);
                System.out.println("Saque realizado com sucesso para os dados bancarios:"
                        + "Agencia: " + saca.getAgencia()
                        + "  Conta: " + saca.getConta()
                        + " DAC: " + saca.getDac()
                        + " Valor: " + "R$" + saca.getValor());
            } else {

                Conta deposita = new Conta();

                deposita.setAgencia(1500);
                deposita.setConta(22);
                deposita.setDac("9");
                deposita.setValor((float) 90.0);
                System.out.println("Valor depositado com sucesso:"
                        + "Agencia: " + deposita.getAgencia()
                        + "  Conta: " + deposita.getConta()
                        + " DAC: " + deposita.getDac()
                        + " Valor: " + "R$" + deposita.getValor());
            }
        } else {
            System.out.println("Desculpe você não tem idade suficiente para prosseguir ='(");
        }

    }


}
