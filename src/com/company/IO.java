package com.company;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO {

    public static void   imprimirMensagemInicial() {

        System.out.println("Bem vindo ao Banco Digital");
    }

    //DTO --DATA TRANSFER OBJECT

    public static Map<String, String> solicitarDados() {


        //Scanner permite inclui dados
        Scanner scanner = new Scanner(System.in);


        System.out.println("Informe seu nome: ");
        String nome = scanner.nextLine();

        System.out.println("Informe seu CPF: ");
        String cpf = scanner.nextLine();


        System.out.println("Informe sua idade: ");
        String idade = scanner.nextLine();

        System.out.println("Informe 1:Saque ou 2: Deposito ");
        String escolha = scanner.nextLine();

        Map<String, String> dados = new HashMap<>();
        dados.put("nome",nome);
        dados.put("cpf",cpf);
        dados.put("idade",idade);
        dados.put("escolha",escolha);


        return   dados;


    }

}
