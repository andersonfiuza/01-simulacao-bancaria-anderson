package com.company;

public class Cliente {

    private String nome;
    private String cpf;
    private String endereco;
    private Integer idade;
    private Integer escolha;


    public Cliente() {
    };

    public Cliente(String nome, String cpf, int idade, int escolha  ) {
        this.nome = nome;
        this.cpf = cpf;
        this.idade = idade;
        this.escolha = escolha;
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public boolean verificaiIdade() {
        if (idade >= 18) {
            return true;
        } else {
            return false;
        }

    }

    public boolean verificaAcao() {
        if (escolha == 1 ) {
            return true;
        } else {
            return false;
        }

    }

}
